# Space Invaders

Space Invaders by [Octopus in vitro](https://github.com/octopusinvitro).

Initial scaffolding generated with [generator-gamejam](https://github.com/belen-albeza/generator-gamejam/).


## Installation


### Requirements

This game uses [gulp](http://gulpjs.com/) for builds and tasks automation.

You can install gulp with npm:

```bash
$ npm install -g gulp
```


### Build

Clone this repository and install dependencies:

```bash
$ git clone git@github.com:octopusinvitro/space-invaders.git
$ cd space-invaders
$ npm install
```

To **build** the game, run the `default` task from the project root:

```bash
$ gulp
```

The `dist` folder will be created, containing a build of the game. You can then start a local server that serves this directory statically to play the game in local:

```bash
$ npm install -g http-server
$ http-server dist
```

You can **clean up** the temporary files (`.tmp/` and `.publish/`) by running:

```bash
$ gulp clean
```

## Development

This project uses [Browserify](http://browserify.org) to handle JavaScript modules.

There is a task that will automatically run Browserify when a JavaScript file changes, and it will also reload the browser.

```bash
$ gulp run
```


## Contribute

You can find this repo at [github](https://github.com/octopusinvitro/space-invaders), [bitbucket](https://bitbucket.com/octopusinvitro/space-invaders) and [gitlab](https://gitlab.com/me-stevens/space-invaders).



## License

[![License](https://img.shields.io/badge/gnu-license-green.svg?style=flat)](https://opensource.org/licenses/GPL-2.0)
GNU License
